<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\Trabajadores;
use app\models\FormularioCorreo;



class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $modelos=Trabajadores::find()->all();
        $arrays=  \yii\helpers\ArrayHelper::toArray($modelos,[
            'app\models\Trabajadores'=> [
                'titulo'=>'nombre',
                'texto'=>function($modelos){
                    return $modelos->nombre . " " . $modelos->apellidos;
                },
                'valores'=>function($modelos){
                    return \yii\helpers\ArrayHelper::toArray($modelos,[
                       'app\models\Trabajadores'=>[
                           'fechaNacimiento',
                           'foto',
                           'delegacion',
                       ] 
                    ]);
                }
                
            ],
        ]);
        
        return $this->render('index',[
            'datos'=>$arrays,
        ]);
    }
    
    public function actionCorreo(){
        $model = new FormularioCorreo();
        if ($model->load(Yii::$app->request->post()) && $model->enviar(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('correo');
            return $this->render('correo');
        }
        return $this->render('correo', [
            'model' => $model,
        ]);
            
    }

}
