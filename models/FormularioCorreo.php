<?php

namespace app\models;

use Yii;
use yii\base\Model;

class FormularioCorreo extends Model
{
    public $correo;
    public $asunto;
    public $contenido;

    public function rules()
    {
        return [
            [['correo', 'asunto', 'contenido'], 'required','message'=>'El {attribute} es obligatorio'],
            ['correo', 'email'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels(){
        return [
            'correo'=>'Escribe el correo del destinatario',
            'asunto'=>'Asunto del correo',
            'contenido'=>'Contenido del correo',
        ];
    }

    public function enviar($remitente){
        if ($this->validate()){
            Yii::$app->mailer->compose()
                ->setTo($this->correo)
                ->setFrom($remitente)
                ->setSubject($this->asunto)
                ->setTextBody($this->contenido)
                ->send();
            return true;
        }
        return false;
    }
}
