<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Enviar Correo';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('correo')){ ?>

        <div class="alert alert-success">
            Correo ennviado correctamente
        </div>

        
    <?php }else{ ?>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'formulario']); ?>

                    <?= $form->field($model, 'correo') ?>

                    <?= $form->field($model, 'asunto') ?>

                    <?= $form->field($model, 'contenido')->textarea(['rows' => 6]) ?>


                    <div class="form-group">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php } ?>
</div>