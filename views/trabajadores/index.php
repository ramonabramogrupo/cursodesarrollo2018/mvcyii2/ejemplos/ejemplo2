<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrabajadoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trabajadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trabajadores-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Trabajadores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php
    \yii\bootstrap\Modal::begin([
    'header' => '<h2>Hello world</h2>',
    'toggleButton' => ['label' => 'click me'],
]);

echo 'Say hello...';

\yii\bootstrap\Modal::end();
?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'apellidos',
            'fechaNacimiento',
            'foto',
            [
              'attribute'=>'nombredelegacion',
              'value'=>'delegacion0.nombre',  
            ],
            [
              'attribute'=>'poblacion',
              'value'=>'delegacion0.poblacion',  
            ],
            'delegacion0.direccion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    

</div>
