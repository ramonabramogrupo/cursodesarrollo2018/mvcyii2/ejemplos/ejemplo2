<?php

namespace app\widgets;

use Yii;

class Noticias extends \yii\base\Widget {

    public $noticias;

    public function run() {
        echo '<div class="row row-flex row-flex-wrap">';
        foreach($this->noticias as $noticia){
            echo $this->render("_noticia", [
                    "titulo" => $noticia["titulo"],
                    "texto" => $noticia["texto"],
                    "valores"=>$noticia["valores"]
        ]);
        }
        echo "'</div>'";
        
    }

}
